SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`trainTypes`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`trainTypes` (
  `trainTypeID` INT NOT NULL ,
  `speed` INT NULL ,
  `maxCars` INT NULL ,
  `purchasePrice` INT NULL ,
  `resalePrice` INT NULL ,
  `fuelEfficiency` INT NULL ,
  PRIMARY KEY (`trainTypeID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`city`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`city` (
  `cityID` INT NOT NULL ,
  `cityLevel` INT NULL ,
  `timeLastPickup` DATETIME NULL ,
  `cityName` VARCHAR(100) NULL ,
  PRIMARY KEY (`cityID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`trains`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`trains` (
  `trainID` INT NOT NULL ,
  `trainType` INT NULL ,
  `milesTraveled` INT NULL ,
  `xPos` INT NULL ,
  `yPos` INT NULL ,
  `destinationCityID` INT NULL ,
  PRIMARY KEY (`trainID`) ,
  INDEX `trainType` (`trainType` ASC) ,
  INDEX `cityID` (`destinationCityID` ASC) ,
  CONSTRAINT `trainType`
    FOREIGN KEY (`trainType` )
    REFERENCES `mydb`.`trainTypes` (`trainTypeID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `cityID`
    FOREIGN KEY (`destinationCityID` )
    REFERENCES `mydb`.`city` (`cityID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`carTypes`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`carTypes` (
  `carTypeID` INT NOT NULL ,
  `maxCapacity` INT NULL ,
  `cargo` TINYINT(1) NULL ,
  `carName` VARCHAR(100) NULL ,
  `purchasePrice` INT NULL ,
  `resalePrice` INT NULL ,
  PRIMARY KEY (`carTypeID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`car`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`car` (
  `carID` INT NOT NULL ,
  `carType` INT NULL ,
  `trainID` INT NULL ,
  PRIMARY KEY (`carID`) ,
  INDEX `carType` (`carType` ASC) ,
  INDEX `trainID` (`trainID` ASC) ,
  CONSTRAINT `carType`
    FOREIGN KEY (`carType` )
    REFERENCES `mydb`.`carTypes` (`carTypeID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `trainID`
    FOREIGN KEY (`trainID` )
    REFERENCES `mydb`.`trains` (`trainID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`userStats`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`userStats` (
  `id` INT NOT NULL ,
  `money` INT NULL ,
  `totalFuelPrice` INT NULL ,
  `hoursPlayed` INT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`cargoTypes`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`cargoTypes` (
  `cargoTypeID` INT NOT NULL ,
  `imageNo` INT NULL ,
  `name` VARCHAR(45) NULL ,
  `invoiceCost` INT NULL ,
  PRIMARY KEY (`cargoTypeID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`cargo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`cargo` (
  `cargoID` INT NOT NULL ,
  `cargoType` INT NULL ,
  `carPosition` INT NULL ,
  `carID` INT NULL ,
  PRIMARY KEY (`cargoID`) ,
  INDEX `cargoType` (`cargoType` ASC) ,
  INDEX `carID` (`carID` ASC) ,
  CONSTRAINT `cargoType`
    FOREIGN KEY (`cargoType` )
    REFERENCES `mydb`.`cargoTypes` (`cargoTypeID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `carID`
    FOREIGN KEY (`carID` )
    REFERENCES `mydb`.`car` (`carID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`passengerTypes`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`passengerTypes` (
  `passengerTypeID` INT NOT NULL ,
  `name` VARCHAR(45) NULL ,
  `imageNo` INT NULL ,
  PRIMARY KEY (`passengerTypeID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`passengers`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`passengers` (
  `passengerID` INT NOT NULL ,
  `passengerType` INT NULL ,
  `seatNo` INT NULL ,
  `carID` INT NULL ,
  PRIMARY KEY (`passengerID`) ,
  INDEX `passengerType` (`passengerType` ASC) ,
  INDEX `carID` (`carID` ASC) ,
  CONSTRAINT `passengerType`
    FOREIGN KEY (`passengerType` )
    REFERENCES `mydb`.`passengerTypes` (`passengerTypeID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `carID`
    FOREIGN KEY (`carID` )
    REFERENCES `mydb`.`car` (`carID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
